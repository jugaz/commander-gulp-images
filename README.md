# Commander Gulp Images Dynamic


<p>It is a small project created by the commander to compile jpg,png and webp tasks.</p>
 
![MIT License](https://img.shields.io/badge/lincense-MIT-yellow?style=for-the-badge) 
![npm: version (tag)](https://img.shields.io/badge/npm-v6.4.3-blue?style=for-the-badge)
![gulp: version (tag)](https://img.shields.io/badge/gulp-v3.9.1-orange?style=for-the-badge)
![node License](https://img.shields.io/badge/node-v8.16.0-green?style=for-the-badge) 


[![GitHub stars](https://img.shields.io/github/stars/jugaz12/gulp-styles?style=social)](https://github.com/jugaz12/gulp-styles)
[![GitHub forks](https://img.shields.io/github/forks/jugaz12/gulp-styles?style=social)](https://github.com/jugaz12/gulp-styles/network)

## Installation

```bash
$ npm install comander-gulp-images
```


#### Command to Compile

```bash
$ compile-images jpgtopng 'entry' --jtp 'ouput' 
```

```bash
$ compile-images webp 'entry' --wp 'ouput' 
```

#### Example

```bash
$ compile-images jpgtopng 'frontend/static/styles/**/*.+(jpg|png)' --jtp 'build/css'
```

```bash
$ compile-images webp 'app/css/**/*.webp' --wp 'build/css'
```
